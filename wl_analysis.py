import numpy as np
import pandas as pd
import scipy.optimize as opt
import matplotlib as mpl
import matplotlib.pyplot as plt
import emcee
import corner
import pickle as pickle

from confidence import find_confidence

posdat = pd.read_excel('data/labdata.xlsx', 0)


posdat = posdat.dropna(how='any')
posdat = posdat.rename(columns={'Position (inches)':'dist'})

def psmod(theta, x):
    wl, phase, amp, sig = theta
    return amp * np.exp(1.j*( (x/(wl * np.cos((52.5/180.)*np.pi))) + phase))

def lnlike(theta, x, meas):
    wl, phase, amp, sig = theta
    mod = psmod(theta, x)
    chi2 = np.sum(np.abs(meas-mod)**2 / sig**2 + np.log(sig**2))
    return -chi2/2.

def lnprior(theta):
    wl, phase, amp, sig = theta
    good = (0.25<wl) & (wl<1.5)
    good = good & (0.0<phase) & (phase<2.*np.pi)
    good = good & (0.0<amp) & (amp<100.0)
    good = good & (0.0<sig) & (sig<15.0)
    if good:
        return 0.0
    else:
        return -np.inf

def lnprob(theta, x, meas):
    lp = lnprior(theta)
    ll = lnlike(theta, x, meas)
    if not np.isfinite(lp+ll):
        return -np.inf
    return lp+ll

x = np.array(posdat['dist'], dtype=float)
meas = np.array(posdat['I'], dtype=float) + 1.j*np.array(posdat['Q'], dtype=float)

guess = np.array([1.0, np.pi, 1.0, 1.0])
orders = guess*0.1
ndim=4
nwalkers = 100
burn = 1000
nsteps = 5000
pos = [guess + (orders*np.random.randn(ndim)) for i in range(nwalkers)]

sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
        args=(x,meas))

sampler.run_mcmc(pos, nsteps)

chain = sampler.chain[:, burn:, :].reshape((-1, ndim))

trifig = corner.corner(chain, labels=['wl', 'phase', 'amp', 'sig'])
trifig.savefig('plots/wl_tri.png', dpi=300)

wl = chain[:,0]

wl_ptile = np.percentile(wl, [16, 50, 84])
print("found wl median/16-84th errors: {0:.5f} -{1:.5f} +{2:.5f}"
    .format(wl_ptile[1],
    wl_ptile[1]-wl_ptile[0],
    wl_ptile[2]-wl_ptile[1]))

wl_conf = find_confidence(wl, crange=0.684)
print("found wl confidence interval: {0:.5f} -{1:.5f} +{2:.5f}"
    .format(wl_conf[2],
    wl_conf[2]-wl_conf[0],
    wl_conf[1]-wl_conf[2]))

plt.figure()
plt.plot(x, np.imag(meas), ls ='None', marker='*', label='imag', color='b')
plt.plot(x, np.real(meas), ls ='None', marker='*', label='real', color='g')
xs = np.linspace(x.min(), x.max(), 1000)
for i in np.random.randint(chain.shape[0], size=100):
    mod = psmod(chain[i], xs)
    plt.plot(xs, mod.real, alpha=0.1, color='g', zorder=-1)
    plt.plot(xs, mod.imag, alpha=0.1, color='b', zorder=-1)
plt.legend(loc='lower right')
plt.xlabel('separation (inches)')
plt.ylabel('visibility')
plt.savefig('plots/wl.png', dpi=300)
