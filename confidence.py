import numpy as np

def find_confidence(dist=None, ps=None, ygrid=None, crange=0.684, bins=None):
    if dist is not None:
        ymin, ymax = dist.min(), dist.max()
        n = dist.size
    
        if bins is not None:
            mgrid = bins
            kgrid = bins
        else:
            mgrid = n/1000
            kgrid = n/1000
    
        mgrid = np.arange(1, mgrid+2, 1, dtype=int)
        ygrid = ymin + ( ((mgrid-1.)/(mgrid.max()-1.)) * (ymax-ymin) )
        ps, _ = np.histogram(dist, bins=ygrid, normed=True)
        ygrid = ygrid[:-1]
        mgrid = mgrid[:-1]
    else:
        mgrid = np.arange(1, ygrid.size+1, 1, dtype=int)
        kgrid = ygrid.size
        ymin, ymax = ygrid.min(), ygrid.max()
        totps = np.sum(ps*(ymax-ymin)/(mgrid.size-1))
        ps = ps/totps
    
    pmax = ps.max()
    ybest = ygrid[np.argmax(ps)]
    
    kgrid = np.arange(1, kgrid+1, 1)
    pgrid = ((kgrid-1)/(kgrid.max()-1)) * pmax
    
    cs = [ np.sum([ps[i] if ps[i]>pgrid[j] else 0.0 for i in mgrid[1:-1]]) for j in range(kgrid.size) ]
    cs = np.array(cs) * (ymax-ymin)/(mgrid.size - 1.)
    co = crange
    for i in range(cs.size):
        if cs[i]>co:
            continue
        else:
            j = i-1
            break
    po = ((pgrid[j+1] - pgrid[j]) / (cs[j+1] - cs[j])) * (co-cs[j]) + pgrid[j]

    for i in range(ygrid.size):
        if ps[i] < po:
            continue
        else:
            j = i-1
            break
    ylow = ((ygrid[j+1]-ygrid[j]) / (ps[j+1]-ps[j])) * (po-ps[j]) + ygrid[j]

    for i in range(ygrid.size-1, 0, -1):
        if ps[i] < po:
            continue
        else:
            j = i
            break
    yup = ((ygrid[j]-ygrid[j-1]) / (ps[j]-ps[j-1])) * (po-ps[j]) + ygrid[j-1]
    
    return ylow, yup, ybest
