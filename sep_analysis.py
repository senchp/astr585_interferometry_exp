import numpy as np
import pandas as pd
import scipy.optimize as opt
import matplotlib as mpl
import matplotlib.pyplot as plt
import emcee
import corner
import pickle as pickle

from confidence import find_confidence

satdat = pd.read_excel('data/labdata.xlsx', 1)

satdat = satdat.dropna(how='any')
satdat = satdat.rename(columns={'Distance of dishes (inches)':'dist',
                        'LCP': 'lcp_i', 'Unnamed: 2': 'lcp_q',
                        'RCP': 'rcp_i', 'Unnamed: 4': 'rcp_q'})

# first, correct for phase offsets using calibrator:
cal = np.array(satdat['rcp_i'], dtype=float) + 1.j*np.array(satdat['rcp_q'], dtype=float)
meas = np.array(satdat['lcp_i'], dtype=float) + 1.j*np.array(satdat['lcp_q'], dtype=float)
meas /= cal
# meas /= np.exp(1.j*np.angle(cal))

def pointsmod(theta, u):
    alpha0, amp, rat, sig = theta
    mod = amp * (1. + rat*np.exp(-2.*np.pi*1.j*alpha0*u))
    return mod

def lnlike(theta, u, meas):
    alpha0, amp, rat, sig = theta
    mod = pointsmod(theta, u)
    chi2 = np.sum(np.abs(meas-mod)**2 / sig**2 + np.log(sig**2))
    return -chi2/2.

def lnprior(theta):
    alpha0, amp, rat, sig = theta
    good = (0<alpha0) & (alpha0<0.008)
    good = good & (0.0<amp) & (amp<10.0)
    good = good & (0.0<rat) & (rat<100.0)
    good = good & (0.0<sig) & (sig<0.1)
    if good:
        return 0.0
    else:
        return -np.inf

def lnprob(theta, u, meas):
    lp = lnprior(theta)
    ll = lnlike(theta, u, meas)
    if not np.isfinite(lp+ll):
        return -np.inf
    return lp+ll

u = np.array(satdat['dist'], dtype=float) / 0.95

guess = np.array([0.003, 5.0, 1.0, 0.005])
orders = guess*0.1
ndim=4
nwalkers = 100
burn = 1000
nsteps = 5000
pos = [guess + (orders*np.random.randn(ndim)) for i in range(nwalkers)]

sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
        args=(u,meas))

sampler.run_mcmc(pos, nsteps)

chain = sampler.chain[:, burn:, :].reshape((-1, ndim))

trifig = corner.corner(chain, labels=['alpha0', 'amp', 'rat', 'sig'])
trifig.savefig('plots/tri.png', dpi=300)

alpha0 = chain[:,0]

alpha0_ptile = np.percentile(alpha0, [16, 50, 84])
print("found alpha0 median/16-84th errors: {0:.5f} -{1:.5f} +{2:.5f}"
    .format(alpha0_ptile[1],
    alpha0_ptile[1]-alpha0_ptile[0],
    alpha0_ptile[2]-alpha0_ptile[1]))

alpha0_conf = find_confidence(alpha0, crange=0.684)
print("found alpha0 confidence interval: {0:.5f} -{1:.5f} +{2:.5f}"
    .format(alpha0_conf[2],
    alpha0_conf[2]-alpha0_conf[0],
    alpha0_conf[1]-alpha0_conf[2]))

plt.figure()
plt.plot(u, np.imag(meas), ls ='None', marker='*', label='imag', color='b')
plt.plot(u, np.real(meas), ls ='None', marker='*', label='real', color='g')
us = np.linspace(20., 200., 1000)
for i in np.random.randint(chain.shape[0], size=100):
    mod = pointsmod(chain[i], us)
    plt.plot(us, mod.real, alpha=0.1, color='g', zorder=-1)
    plt.plot(us, mod.imag, alpha=0.1, color='b', zorder=-1)
plt.xlim(20, 200)
plt.gca().autoscale(False)
plt.legend(loc='lower right')
plt.xlabel('separation (inches)')
plt.ylabel('calibrated visibility (arbitrary units)')
plt.savefig('plots/satsep.png', dpi=300)
